package com.example.user.controllers;


import com.example.user.entity.User;
import com.example.user.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")

public class UserController {
	
    @Autowired
    private final UserService userServices;

    public UserController(UserService userServices) {
        this.userServices = userServices;
    }

    @PostMapping
    public User postFirstNameUser(@RequestBody User user){
       return userServices.createNewUser(user);
    } 
}


