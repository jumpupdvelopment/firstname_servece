package com.example.user.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.user.entity.User;

public interface UserRepo extends MongoRepository <User, String> {
}
