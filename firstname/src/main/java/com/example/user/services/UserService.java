package com.example.user.services;

import com.example.user.entity.User;
import com.example.user.repo.UserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserService {

    @Value("${midname.url}")
    private String midnameUrl;
    @Autowired
    private final UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }
    private static final RestTemplate restTemplate = new RestTemplate();

    public User createNewUser(User user){

        RequestEntity<Object> requestEntity = RequestEntity.put(midnameUrl).body(user);
        ResponseEntity<User> response = restTemplate.exchange(requestEntity, User.class);
        User newUser = response.getBody();
        newUser.setFirstName(user.getFirstName());
        return userRepo.save(newUser);
    }
    
    
    
}
