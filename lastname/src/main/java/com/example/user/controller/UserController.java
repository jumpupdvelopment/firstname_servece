package com.example.user.controller;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.user.model.User;

@RestController
@RequestMapping("/")
public class UserController {

    @PutMapping
    public User getLastName (@RequestBody User user){
        User newUser = new User();
        newUser.setLastName(user.getLastName());
        return newUser;
    }
}
