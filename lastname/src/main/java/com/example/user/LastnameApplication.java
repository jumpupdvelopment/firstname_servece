package com.example.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LastnameApplication {

	public static void main(String[] args) {
		SpringApplication.run(LastnameApplication.class, args);
	}

}
