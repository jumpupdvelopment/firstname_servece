package com.example.user.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.user.entity.User;

public interface UserRepo extends MongoRepository<User, String> {
}
