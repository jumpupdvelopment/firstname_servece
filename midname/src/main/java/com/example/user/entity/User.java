package com.example.user.entity;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class User {

	@Id
	private String id;
    private String firstName;
    private String lastName;
    private String midName;

}