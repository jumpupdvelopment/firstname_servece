package com.example.user.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.user.entity.User;

@Service
public class UserService {
    private static final RestTemplate restTemplate = new RestTemplate();

    @Value("${lastname.url}")
    private String lastnameUrl;

    public User returnUser(User user) {
        RequestEntity<Object> requestEntity = RequestEntity.put(lastnameUrl).body(user);
        ResponseEntity<User> response = restTemplate.exchange(requestEntity, User.class);
        User newUser = response.getBody();
        newUser.setMidName(user.getMidName());
        return newUser;
    }

}
